#!/bin/sh

cd ../../../target

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)   
    case "$KEY" in
            REPOSITORY_NAME)    REPOSITORY_NAME=${VALUE} ;;     
            *)   
    esac    
done

java -jar crawler-0.0.1-SNAPSHOT-jar-with-dependencies.jar $REPOSITORY_NAME