package com.qwil.th.crawler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.eclipse.egit.github.core.Contributor;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.RepositoryId;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.RepositoryService;

public class App {
	public static void main(String[] args) {
		Utils utils = new Utils();
		try {
			if (args.length < 1) {
				throw new IllegalArgumentException("Argument is not provided, e.g. https://github.com/kubernetes/kubernetes");
			}

			// validating the git url
			String gitUrl = args[0];
			if (!utils.validateGitUrl(gitUrl)) {
				throw new IllegalArgumentException("url is not valid, e.g. https://github.com/kubernetes/kubernetes");
			}

			// Parsing the owner and name of repo from the url
			String owner = utils.getRepoOwner(gitUrl);
			String name = utils.getRepoName(gitUrl);

			// loading properties file
			Properties prop = utils.loadProps("config.properties");

			// Setting up a git client
			GitHubClient client = new GitHubClient();
			String user = prop.getProperty("user");
			String password = prop.getProperty("password");
			client.setCredentials(user, password);
			RepositoryService service = new RepositoryService(client);

			// Getting the repository from owner and name
			RepositoryId repository = new RepositoryId(owner, name);

			// List of all contributors contributing to a repository
			List<Contributor> contributors = service.getContributors(repository, true);
			Map<String, Integer> map = new HashMap<String, Integer>();

			// Iterating through all contributors
			if (!contributors.isEmpty()) {
				for (Contributor contributor : contributors) {
					String login = contributor.getLogin();
					if (login != null) {
						List<Repository> repos = service.getRepositories(login);
						map = utils.getRepoCount(map, repos);
					}
				}
			}

			// sorting the map of repo and count
			TreeMap<String, Integer> result = utils.sortByRepoCount(map);

			// printing the top n number of repos
			if (result != null && !result.isEmpty()) {
				utils.printTopRepos(result, 10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
