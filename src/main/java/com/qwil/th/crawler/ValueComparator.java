package com.qwil.th.crawler;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ValueComparator<K, V extends Comparable<V>> implements Comparator<K> {
	HashMap<String, Integer> map = new HashMap<String, Integer>();

	public ValueComparator(Map<String, Integer> map2) {
		this.map.putAll(map2);
	}

	public int compare(K s1, K s2) {
		return -map.get(s1).compareTo(map.get(s2));// descending order
	}
}
