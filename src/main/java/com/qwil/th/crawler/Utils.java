package com.qwil.th.crawler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import org.eclipse.egit.github.core.Repository;

public class Utils {
	// method to generate a map of repo and its occurence
	public Map<String, Integer> getRepoCount(Map<String, Integer> map, List<Repository> repos) {
		if (!repos.isEmpty()) {
			for (Repository repo : repos) {
				if (!map.containsKey(repo.getName())) {
					map.put(repo.getName(), 1);
				} else {
					map.put(repo.getName(), map.get(repo.getName()) + 1);
				}
			}
		}
		return map;
	}

	// method to sort a map based on value
	public TreeMap<String, Integer> sortByRepoCount(Map<String, Integer> map) {
		TreeMap<String, Integer> sortedMap = null;
		if (!map.isEmpty()) {
			Comparator<String> comparator = new ValueComparator<String, Integer>(map);
			sortedMap = new TreeMap<String, Integer>(comparator);
			sortedMap.putAll(map);
		}
		return sortedMap;
	}

	// method to print first n records of a map
	public void printTopRepos(Map<String, Integer> sortedMap, int n) {
		if(n>0) {
			int count = 1;
			Set<String> keys = sortedMap.keySet();
			for (String key : keys) {
				System.out.println("Repository:" + key + " Count:" + sortedMap.get(key));
				if (count == n) {
					break;
				}
				count++;
			}
		} else {
			throw new IllegalArgumentException("arguments provided are not correct i.e. n should be a positive integer, e.g. printTopRepos(map, 4) ");
		}
		
	}

	public Properties loadProps(String filename) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = App.class.getClassLoader().getResourceAsStream("config.properties");
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}

	public int countOccurence(String str, char c) {
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == c) {
				count++;
			}
		}
		return count;
	}

	public boolean validateGitUrl(String url) {
		if (url.contains("https://github.com/") && (url.substring(19).length() > 2) && url.substring(19).contains("/")
				&& (countOccurence(url.substring(19), '/') == 1)) {
			return true;
		} else {
			return false;
		}
	}

	public String getRepoName(String url) {
		String[] arrOfStr = url.split("/");
		return arrOfStr[arrOfStr.length - 1];
	}

	public String getRepoOwner(String url) {
		String[] arrOfStr = url.split("/");
		return arrOfStr[arrOfStr.length - 2];
	}
}
