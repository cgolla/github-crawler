package com.qwil.th.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.Assert;

import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.RepositoryService;

public class UtilsTest {
	static RepositoryService service;
	static Utils utils;
	
	@BeforeClass
	public static void initialize() {
		utils= new Utils();
		Properties prop = utils.loadProps("config.properties");
		GitHubClient client = new GitHubClient();
		String user = prop.getProperty("user");
		String password = prop.getProperty("password");
		client.setCredentials(user, password);
		service = new RepositoryService(client);
    }
	
	@Test
	public void getRepoCountShouldReturnEmpty() {
		Map<String, Integer> map = utils.getRepoCount(new HashMap<String, Integer>(), new ArrayList<Repository>());
		Assert.assertTrue(map.isEmpty());
	}
	
	@Test
	public void getRepoCountShouldReturnNonNull() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<Repository> list = null;
		try {
			list = service.getRepositories("chaitanyatejagolla");
		} catch (IOException e) {
			e.printStackTrace();
		}
		map = utils.getRepoCount(map, list);
		Assert.assertEquals(1, map.get("MovieViewer").intValue());
	}
	
	@Test
	public void sortByRepoCountShouldReturnNonNull() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("E", 5);
		map.put("F", 6);
		map.put("A", 1);
		map.put("B", 2);
		TreeMap<String, Integer> sortedMap = utils.sortByRepoCount(map);
		Assert.assertEquals("F", sortedMap.firstKey());
	}
	
	@Test
	public void sortByRepoCountShouldReturnNull() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		TreeMap<String, Integer> sortedMap = utils.sortByRepoCount(map);
		Assert.assertNull(sortedMap);
	}
	
	@Test
	public void countOccurenceShouldReturnNonNull() {
		int count = utils.countOccurence("kubernetes/kubernetes/app", '/');
		Assert.assertEquals(2, count);
	}
	
	@Test
	public void validateGitUrlShouldReturnTrue() {
		Assert.assertTrue(utils.validateGitUrl("https://github.com/kubernetes/kubernetes"));
		Assert.assertTrue(utils.validateGitUrl("https://github.com/k/k"));
	}
	
	@Test
	public void validateGitUrlShouldReturnFalse() {
		Assert.assertFalse(utils.validateGitUrl("https://github.com/kubernetes/kubernetes/"));
		Assert.assertFalse(utils.validateGitUrl("https://github.com"));
		Assert.assertFalse(utils.validateGitUrl("https://github.com/kubernetes"));
		Assert.assertFalse(utils.validateGitUrl("https://google.com/owner/repo"));
	}
	
	@Test
	public void getRepoOwnerShouldReturnNonNull() {
		String repoOwner = utils.getRepoOwner("kohsuke/github-api");
		Assert.assertEquals("kohsuke", repoOwner);
	}
	
	@Test
	public void getRepoNameShouldReturnNonNull() {
		String repoName = utils.getRepoName("kohsuke/github-api");
		Assert.assertEquals("github-api", repoName);
	}
	
}
